import smtplib
from email.mime.text import MIMEText


class Xend:

    def __init__(self, username=None, password=None, recipient=None, content=None):
        # Credentials
        self.username = username
        self.password = password

        self.recipient = recipient

        # Email Content file location
        self.content = content

    def load_content(self, content=None):
        if content:
            self.msg = MIMEText(content)
            self.content = self.msg
        else:
            assert self.content
            f = open(self.content, 'rb')
            self.msg = MIMEText(f.read())
            f.close()

    def attach_file(self):
        pass

    def details(self):
        print "Username: %s" % self.username
        print "Password: %s" % self.password
        print "Recipient: %s" % self.recipient
        print "Content: %s" % self.msg
        print "Attachment: %s" % self.attach_file()

    def send(self):
        assert self.username.endswith('@gmail.com')
        assert self.password
        assert self.recipient.endswith('.com')
        assert self.content == True

        # Email details
        self.msg['Subject'] = 'Email from %s | Xend' % self.username
        self.msg['From'] = self.username
        self.msg['To'] = self.recipient

        # Send the message via gmail SMTP server
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.starttls()
        server.login(self.username, self.password)
        server.sendmail(self.username, [self.recipient], self.msg.as_string())
        server.quit()
        return True # if mail sent successfully


if __name__ == '__main__':
    mail = Xend()
    mail.username = raw_input("Enter your email: ")
    mail.password = raw_input("Enter password: ")
    mail.recipient = raw_input("Send email to: ")
    message = raw_input("Enter message to send to %s: " % mail.recipient)
    mail.load_content(message)
    print "\nConfirm email details:\n"
    mail.details()
    confirm = raw_input("%s\nSend email? <Y/n>:" % ('*'*60))
    if confirm.lower() == 'y':
        mail.send()
        print "Sent successfully."
    print "Exiting program..."
