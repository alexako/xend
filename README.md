Xend
====

A simple script that reads a given text input and sends it as an email 
to a given recipient using the Gmail SMTP server.

Requires a Gmail username and password.


#####Example usage:

```python
mail  = Xend()
mail.username = "<username>@gmail.com"
mail.password = "<your-password>"
mail.recipient = "<username>@example.com"
mail.content = "file/path/to/mailcontent/<filename>"

mail.send()
```
or
```python
username = "<username>@gmail.com"
password = "<your-password>"
recipient = "<username>@example.com"
content = "This is the email contents."

mail = Xend(username, password, recipient, content)
mail.details() # Prints all details of the email

mail.send()
